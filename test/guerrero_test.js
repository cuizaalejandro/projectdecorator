var expect = require('chai').expect;
var Arquero  = require('../arquero');
var Infanteria  = require('../Infanteria');
var Fuego  = require('../Fuego');
var Madera  = require('../Madera');
var Hierro  = require('../Hierro');

describe('guerrero tiene materiales', function () {

  it('cuando un guerrero de tipo infanteria  añade madera deberia devolver  15 el daño ', async function () {
    let arquero  = new Infanteria();
    arquero = new Madera(arquero);
    let esperado = 15; 
    let daño  =arquero.CalcularDano();
    expect(daño).equals(esperado);
  });
  it('cuando un guerrero de tipo infanteria añade madera y hierro deberia devolver  25 el daño ', async function () {
    let arquero  = new Infanteria();
    arquero = new Madera(arquero);
    arquero = new Hierro(arquero);
    let esperado = 25; 
    let daño = arquero.CalcularDano();
    expect(daño).equals(esperado);
  });
  it('cuando un guerrero de tipo infanteria añade madera y hierro  y fuego deberia devolver  55 el daño ', async function () {
    let arquero  = new Infanteria();
    arquero = new Madera(arquero);
    arquero = new Hierro(arquero);
    arquero = new Fuego(arquero);
    let esperado = 55; 
    let daño = arquero.CalcularDano();
    expect(daño).equals(esperado);
  });

  it('cuando un guerrero de tipo arquero  añade madera deberia devolver  10 el daño ', async function () {
    let arquero  = new Arquero();
    arquero = new Madera(arquero);
    let esperado = 10; 
    let daño  =arquero.CalcularDano();
    expect(daño).equals(esperado);
  });

  it('cuando un guerrero de tipo arquero  añade madera y hierro deberia devolver  20 el daño ', async function () {
    let arquero  = new Arquero();
    arquero = new Madera(arquero);
    arquero = new Hierro(arquero);
    let esperado = 20; 
    let daño  =arquero.CalcularDano();
    expect(daño).equals(esperado);
  });
  it('cuando un guerrero de tipo arquero  añade madera y hierro y fuegi  deberia devolver  50 el daño ', async function () {
    let arquero  = new Arquero();
    arquero = new Madera(arquero);
    arquero = new Hierro(arquero);
    arquero = new Fuego(arquero);
    let esperado = 50; 
    let daño  =arquero.CalcularDano();
    expect(daño).equals(esperado);
  }); 


});